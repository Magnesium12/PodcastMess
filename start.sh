#!/bin/bash

cd $(dirname $0)

# Upgrade youtube-dl -----
dayOfWeek="$(date +%u)"

if [ -f ./dayOfLastUpdate.last ]
	then dayOfLastUpdate="$(cat ./dayOfLastUpdate.last)"
	else dayOfLastUpdate="NA"
fi

if ([ "$dayOfWeek" == "3" ] || [ "$dayOfWeek" == "7" ]) && [ "$dayOfLastUpdate" != "$(date +%Y%m%d)" ]
	then
	pip3 install youtube-dl --upgrade
	date +%Y%m%d > ./dayOfLastUpdate.last
fi
# -----

./run_payload.sh podcasts/* # Beide gleichwertig
#./run_payload.sh /data/media/Podcasts/0_Feeds_0/*

exit 0
