#!/bin/bash
# Definitionen

testfile="./scriptIsRunning"
logfile="./run_payload.log"

logText() {
	echo -ne "$(date +%x"–"%X)\t$2\n" >> "$1"
}
run_pks() {
	pks="$1"
	raw="$(grep -vP '^#' "$pks")" # Rohdaten einlesen
	# Minimales Intervall zwischen Durchgängen
	if [ -f "$pks".last ]
	then last="$(cat "$pks".last)"
	else last=0
	fi
	minint=$(echo "$raw" | grep -P '^minint:' | awk '{print $2}')
	if [ "$minint" ] && [ "$(($(date +%s)-$last))" -lt "$minint" ]
	then
		log="Overkill:\t$pks zu häufig ausgeführt, wird übersprungen..."
		echo -ne "$log\n"
		logText "$logfile" "$log"
		return 0
	fi

	# Verwendete Methode
	method=$(echo "$raw" | grep -P '^method:' | awk '{print $2}')

	# Die Adresse
	url=$(echo "$raw" | grep -P '^url:' | sed "s|.*\"\(.*\)\".*|\\1|")

	#Speicherort
	save=$(echo "$raw" | grep -P '^save:' | sed "s|.*\"\(.*\)\".*|\\1|")
	if ! [ -d "$save" ]
	then
		log="Fehler:\t\tVerzeichnis \"$save\" existiert nicht, $pks wird übersprungen..."
		echo -ne "$log\n"
		logText "$logfile" "$log"
		return 1
	fi

	# .source Dateien erzeugen?
	source=$(echo "$raw" | grep -P '^source:' | awk '{print $2}')
	if [ "$source" == "yes" -o "$source" == "1" -o "$source" == "ja" -o "$source" == "true" ]
	then source="yes"
	else source="no"
	fi

	# Links erzeugen?
	alert=$(echo "$raw" | grep -P '^alert:' | awk '{print $2}')
	if [ "$alert" == "yes" -o "$alert" == "1" -o "$alert" == "ja" -o "$alert" == "true" ]
	then alert="yes"
	else alert="no"
	fi

	# Andere Dateinamensfindung
	nicename=$(echo "$raw" | grep -P '^nicename:' | awk '{print $2}')
	if [ "$nicename" == "yes" -o "$nicename" == "1" -o "$nicename" == "ja" -o "$alert" == "nicename" ]
	then nicename="yes"
	else nicename="no"
	fi

	# Methoden zum Download
	case $method in
	podcast) # klassische Podcasts
		log="podcast:\t$pks"
		echo -ne "$log\n"
		logText "$logfile" "$log"
		./bashpodder.sh "$url" "$save" "$source" "$alert" "$nicename"
	;;
	7tv) # 7tv Einträge
		log="7tv:\t\t$pks"
		echo -ne "$log\n"
		logText "$logfile" "$log"
		./get7tv.sh "$url" "$save" "$source" "$alert" "$nicename"
	;;
	TELE5) # zdf Einträge
		# Andere Dateinamensfindung
		sstring=$(echo "$raw" | grep -P '^sstring:' | sed "s|.*\"\(.*\)\".*|\\1|")
		pattern=$(echo "$raw" | grep -P '^pattern:' | sed "s|.*\"\(.*\)\".*|\\1|")
		log="TELE5:\t\t$pks"
		echo -ne "$log\n"
		logText "$logfile" "$log"
		if  [ "$sstring" ]
		then ./getTELE5.sh "$url" "$save" "$source" "$alert" "$nicename" "$pattern" "$sstring"
		else ./getTELE5.sh "$url" "$save" "$source" "$alert" "$nicename" "$pattern"
		fi
	;;
	zdf) # zdf Einträge
		# Andere Dateinamensfindung
		sstring=$(echo "$raw" | grep -P '^sstring:' | sed "s|.*\"\(.*\)\".*|\\1|")
		pattern=$(echo "$raw" | grep -P '^pattern:' | sed "s|.*\"\(.*\)\".*|\\1|")
		log="zdf:\t\t$pks"
		echo -ne "$log\n"
		logText "$logfile" "$log"
		if  [ "$sstring" ]
		then ./getZDF.sh "$url" "$save" "$source" "$alert" "$nicename" "$pattern" "$sstring"
		else ./getZDF.sh "$url" "$save" "$source" "$alert" "$nicename" "$pattern"
		fi
	;;
	task) # Auftrag aus Datei
		runs=$(echo "$raw" | grep -P '^runs:' | awk '{print $2}')
		log="task:\t\t$pks"
		echo -ne "$log\n"
		logText "$logfile" "$log"
		./runTask.sh "$url" "$save" "$source" "$alert" "$nicename" "$runs"
	;;
	anonymous_task)
		runs=$(echo "$raw" | grep -P '^runs:' | awk '{print $2}')
		log="anonymous_task:\t\t$pks"
		echo -ne "$log\n"
		logText "$logfile" "$log"
		./runAnonymousTask.sh "$url" "$save" "$source" "$alert" "$nicename" "$runs"
	;;
	*)
		echo "unbekannte Methode"
	;;
	esac
	date +%s > "$pks".last
	return 0
}
run_payload() {
	if ! [ "$1" ]
	then
		log="Kein Argument:\tEnde"
		echo -ne "$log\n"
		logText "$logfile" "$log"
		return 1
	fi

	for obj in "$@"
	do
		if ! [ -e "$obj" ]
		then
			log="Fehl. Eingabe:\tEnde"
			echo -ne "$log\n"
			logText "$logfile" "$log"
			return 2
		fi
	done

	for pks in "$@"; do
		if [ "$(echo "$pks" | grep -cP '\.old$|\.bak$|\.w$')" -eq "1" ] # Inactivated?
		then continue
		elif [ -f "$pks" ] # Ist es eine Datei?
		then 
			if [ "$(echo "$pks" | grep -cP '.pks$')" -eq "1" ] # Richtige Endung?
			then run_pks "$pks"
			fi
		elif [ -d "$pks" ] # Ist es ein Ordner?
		then run_payload "$pks"/*
		fi
	done
	return 0
}

# Make script crontab friendly:
#cd $(dirname $0)

if [ -f "$testfile" ]
then
	log="Doppelter Start:\tEnde"
	echo -ne "$log\n"
	logText "$logfile" "$log"
	exit 0
else
	>"$testfile"
fi

run_payload "$@"

rm "$testfile"
exit 0
