#!/bin/bash
# By Linc 10/1/2004
# Find the latest script at http://lincgeek.org/bashpodder
# Revision 1.21 12/04/2008 - Many Contributers!
# If you use this and have made improvements or have comments
# drop me an email at linc dot fessenden at gmail dot com
# and post your changes to the forum at http://lincgeek.org/lincware
# I'd appreciate it!
# Modyfied by Marco Grimmeißen

# Make script crontab friendly:
#cd $(dirname $0)

#Parameter:
if [ "$1" ]
	then urlPodcast="$1"
	else exit 1
fi
if [ "$2" ] && [ -d "$2" ]
	then save="$2"
	else exit 2
fi
if [ "$3" ] && [ "$3" == "yes" ]
	then source="yes"
	else source="no"
fi
if [ "$4" ] && [ "$4" == "yes" ]
	then alert="yes"
	else alert="no"
fi
if [ "$5" ] && [ "$5" == "yes" ]
	then nicename="yes"
	else nicename="no"
fi

# Konfiguration
. ./config.sh
if ! [ "$datadir" -a "$tmpLog" -a "$podcastLog" ]
then
	echo "Script fehlkonfiguriert, Abbruch..."
	exit 3
fi

# datadir is the directory you want podcasts saved to:
#datadir=/data/media/Podcasts/0_Release_0/$(date +%Y-%m-%d)

# create datadir if necessary:
mkdir -p "$datadir"

# Delete any temp file:
rm -f "$tmpLog" #temp.log

file=$(xsltproc parse_enclosure.xsl $urlPodcast 2> /dev/null || wget -q $urlPodcast -O - | tr '\r' '\n' | tr \' \" | sed -n 's/.*\ url="\([^"]*\)".*/\1/p')

for url in $file
	do
	if ! grep -q "$url" "$podcastLog"
		then
		
		if [ "$nicename" == "yes" ]
			then
				name=$(basename "$url")
				name=${name%%\?*?}
			else name=$(echo "$url" | awk -F'/' {'print $NF'} | awk -F'=' {'print $NF'} | awk -F'?' {'print $1'})
		fi
		echo -ne "Speichere:\t""$url"" in ""$name""\n"

		if ! wget -t 10 -c -O "$(readlink -f "$save"/"$name")" "$url"
			then
			echo "Downloadprozess fehlerhaft"
			continue
		fi
		if [ "$alert" == "yes" ]
			then ln "$(readlink -f "$save"/"$name")" "$(readlink -f $datadir)"/"$(date +%y%m%d_%H%M%S)"_"$(basename "$save")"_-_"$name"
		fi
		if [ "$source" == "yes" ]
			then echo "$url" > "$save"/"$name".source
		fi
		echo "$url" >> "$tmpLog"
	fi
done

# Move dynamically created log file to permanent log file:
cat "$podcastLog" >> "$tmpLog"
sort "$tmpLog" | uniq > "$podcastLog"
rm "$tmpLog"
# Create an m3u playlist:
ls "$datadir" | grep -v m3u > "$datadir"/podcast.m3u
