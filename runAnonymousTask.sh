#!/bin/bash
# By Linc 10/1/2004
# Find the latest script at http://lincgeek.org/bashpodder
# Revision 1.21 12/04/2008 - Many Contributers!
# If you use this and have made improvements or have comments
# drop me an email at linc dot fessenden at gmail dot com
# and post your changes to the forum at http://lincgeek.org/lincware
# I'd appreciate it!
# Modyfied by Marco Grimmeißen

# Make script crontab friendly:
#cd $(dirname $0)

#Parameter:
if [ "$1" ]
	then urlPodcast="$1"
	else exit 1
fi
if [ "$2" ] && [ -d "$2" ]
	then save="$2"
	else exit 2
fi
if [ "$3" ] && [ "$3" == "yes" ]
	then source="yes"
	else source="no"
fi
if [ "$4" ] && [ "$4" == "yes" ]
	then alert="yes"
	else alert="no"
fi
if [ "$5" ] && [ "$5" == "yes" ]
	then nicename="yes"
	else nicename="no"
fi
if [ "$6" ] && [ "$6" -gt 0 ]
	then runs="$6"
	else runs=0
fi

# Konfiguration
. ./config.sh
if ! [ "$datadir" -a "$tmpLog" -a "$podcastLog" ]
then
        echo "Script Fehlkonfiguriert, Abbruch..."
        exit 3
fi

# datadir is the directory you want podcasts saved to:
#datadir=/data/media/Podcasts/0_Release_0/$(date +%Y-%m-%d)

# create datadir if necessary:
mkdir -p "$datadir"

# Delete any temp file:
rm -f "$tmpLog"

if ! [ -f "$urlPodcast" ]
	then
	echo -ne "Warnung:\t""$urlPodcast"" existiert nicht, Ende\n"
	exit 0
fi

eplist="$(cat "$urlPodcast")"
count=1

for url in $eplist
	do
	if ! grep -q "$url" "$podcastLog" && ([ "$count" -le "$runs" ] || [ "$runs" -eq 0 ])
		then
		~/restart_tor.sh # You have to create this file by yourself
        echo "TOR REKONFIGURIERT"
        sleep 5s
		
		name="$(torify youtube-dl --get-filename --restrict-filenames -o "S%(season_number)sE%(episode_number)s_-_%(title)s.%(ext)s" "$url")"
		echo -ne "Speichere:\t$url in $(echo "$name" | head -n 1)\n"

		if ! torify youtube-dl --fragment-retries 30 --abort-on-unavailable-fragment --restrict-filenames -o "$(readlink -f "$save"/"$name")" "$url"
			then
				cd "$spwd"
				echo "Downloadprozess fehlerhaft"
				continue
		fi
		
		echo "$url" >> "$tmpLog"
		((count++))
	fi
done

# Move dynamically created log file to permanent log file:
cat "$podcastLog" >> "$tmpLog"
sort "$tmpLog" | uniq > "$podcastLog"
rm "$tmpLog"
# Create an m3u playlist:
ls "$datadir" | grep -v m3u > "$datadir"/podcast.m3u
